#!/bin/zsh

if [ "$#" -ne 1]; then
    echo "Usage: $0 local_image_name"
    exit 1
fi

local_image_name="$1"
shift

image_name=registry.gitlab.com/jleasure/testing-security-products/$local_image_name

docker tag $local_image_name $image_name
docker push $image_name
docker rmi $image_name